const ccxt = require('ccxt')

const setting = {
  speed: 1.5, // second
  binanceApi: '',
  binanceApiSecrete: '',
  exchange: undefined,
  dbDriver: undefined,
  userEmail: 'thanarackc@gmail.com'
}

// let coin = undefined
let coins = []
// const coin = {
//   coinName: 'doge',
//   symbolCode: 'DOGE/USDT',
//   profitPercentage: 3, // profit value per trade. profit is higher. rate value will hight also.
//   todayLowValue: 0, // Lot value 24 hr.zsa 1
//   todayHightValue: 0, // Hight value 24 hr. 10
//   todayValue: 0, // Current value coin 5 usd 5.8
//   profitStack: 0, // Profit stack
//   myWallet: 1000, // 1000 usd
//   isBuy: false, // Limit buy
//   buyAmountPercentage: 20, // Amount by percentage. 100% = Spend all wallet
//   buyAmountCoinValue: 0, // Amount by coin
//   buyAmountPriceValue: 0, // Amount by price
//   buyAtValue: 0, // Buy coin at value
//   buyRateSupplyPercentage: 80, // Start buy coin when minimum today price more than this value, set 0 to buy every price,
//   feeValue: 0.25, // binance fee
//   market: 'binance'
// }

const setupExchange = async () => {
  // const binance = new ccxt.binance()
  // const binamceMarket = binance.loadMarkets()
  const exchangeId = 'binance'
  const exchangeClass = ccxt[exchangeId]
  const exchange = new exchangeClass({
    apiKey: setting.binanceApi,
    secret: setting.binanceApiSecrete,
    timeout: 30000,
    enableRateLimit: true
  })
  setting.exchange = exchange
}

const setupDatabase = async () => {
  const MongoClient = require('mongodb').MongoClient
  const url =
    'mongodb://root:rootpassword@206.189.38.110:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false'
  MongoClient.connect(url, { useUnifiedTopology: true }, async function(
    err,
    db
  ) {
    if (err) throw err
    const dbo = db.db('bot-trade-01')
    // Get user setup
    dbo
      .collection('users')
      .findOne({ userEmail: setting.userEmail }, function(err, result) {
        if (err) throw err
        setting.binanceApi = result.apikey
        setting.binanceApiSecrete = result.apisecret
        setupExchange()
        // Get profile in mongo
        dbo
          .collection('profile')
          .find({ status: 1 })
          .toArray((err, result) => {
            if (err) throw err
            coins = result
            coinPriceUpdate()
          })
      })
    setting.dbDriver = dbo
  })
}

// Get lastes price from host
// const coinPriceUpdate = async () => {
//   if (!setting.exchange) return
//   for (let i = 0; i < coins.length; i++) {
//     const coin = coins[i]
//     const fetchTicker = await setting.exchange.fetchTicker(coin.symbolCode)
//     if (fetchTicker) {
//       coin.todayHightValue = +fetchTicker.info.highPrice
//       coin.todayLowValue = +fetchTicker.info.lowPrice
//       coin.todayValue = +fetchTicker.info.lastPrice
//     }
//   }
// }

const coinPriceUpdate = () => {
  const WebSocket = require('ws')
  let streamName = ''
  coins.map((item, index) => {
    if (item.symbolTicker) {
      streamName += `${item.symbolTicker}@ticker`
    }
    if (index + 1 !== coins.length) {
      streamName += '/'
    }
  })
  console.log(streamName)
  // Binance socker path.
  const ws = new WebSocket(
    `wss://stream.binance.com:9443/stream?streams=${streamName}`
  )
  ws.on('open', function() {
    console.log('connected binanace socket.')
  })
  ws.on('message', function(data) {
    if (data) {
      try {
        const dataJson = JSON.parse(data)
        if (dataJson) {
          setPriceOC(dataJson)
        }
      } catch (e) {
        console.log(e)
      }
    }
  })
}

const setPriceOC = ticker => {
  // Get coin
  const tickerData = ticker.data
  const tksymbol = tickerData.s.toLowerCase()
  const coin = coins.find(t => t.symbolTicker === tksymbol)
  if (coin) {
    try {
      coin.todayHightValue = +tickerData.h
      coin.todayLowValue = +tickerData.l
      coin.todayValue = +tickerData.c
      botTradeV2(coin)
    } catch (e) {
      console.log(e)
    }
  }
  // for (let i = 0; i < coins.length; i++) {
  //   try {
  //     const coin = coins[i]
  //     const tickerData = ticker.data
  //     const tksymbol = tickerData.s.toLowerCase()
  //     const getTicker = tksymbol === coin.symbolTicker
  //     if (getTicker) {
  //       coin.todayHightValue = +tickerData.h
  //       coin.todayLowValue = +tickerData.l
  //       coin.todayValue = +tickerData.c
  //       botTradeV2(coin)
  //     }
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }
}

// const botTradeV1 = async () => {
//   console.log('\n###################################################')
//   for (let i = 0; i < coins.length; i++) {
//     const coin = coins[i]
//     console.log(
//       `Coin: ${coin.coinName}, price: ${coin.todayValue} usd, low: ${coin.todayLowValue} usd, hight: ${coin.todayHightValue} usd, sell at: ${coin.minSellValue} usd`
//     )
//     // return
//     // Update price
//     updateProfilePriceCoin(coin)
//     // const d = new Date()
//     // Mock trade today value
//     // coin.todayValue = Math.floor(Math.random() * coin.todayHightValue)
//     // coin.todayValue += 0.05
//     // if (coin.todayValue >= coin.todayHightValue) return
//     if (coin.todayValue <= 0) return
//     // End mock value
//     // Check should buy ?
//     // And buy between low and height per day
//     // And buy when rate match
//     const getRateAmount = percentage(
//       coin.buyRateSupplyPercentage,
//       coin.todayHightValue
//     )
//     if (
//       coin.myWallet > 0 &&
//       coin.todayValue >= coin.todayLowValue &&
//       coin.todayValue <= coin.todayHightValue &&
//       !coin.isBuy
//     ) {
//       if (getRateAmount !== 0 && coin.todayValue >= getRateAmount) return
//       // Get amount user buy
//       const convertBuytoAmount = percentage(
//         coin.buyAmountPercentage,
//         coin.myWallet
//       )
//       const getFee = percentage(coin.feeValue, convertBuytoAmount)
//       coin.myWallet = Math.abs(coin.myWallet - convertBuytoAmount)
//       coin.buyAtValue = coin.todayValue
//       coin.buyAmountPriceValue = Math.abs(convertBuytoAmount - getFee)
//       coin.buyAmountCoinValue = Math.abs(
//         (convertBuytoAmount - getFee) / coin.todayValue
//       )
//       coin.isBuy = true
//       let minSellValue = percentage(coin.profitPercentage, coin.buyAtValue)
//       minSellValue = Math.abs(minSellValue + coin.buyAtValue)
//       coin.minSellValue = minSellValue
//       // Exchange order
//       try {
//         const buy = await setting.exchange.createOrder(
//           coin.symbolCode,
//           'market',
//           'buy',
//           coin.buyAmountCoinValue
//         )
//         if (buy) {
//           console.log(
//             'BUY',
//             coin.symbolCode,
//             coin.buyAmountCoinValue,
//             coin.todayValue
//           )
//           createTransact(buy)
//           coin.buyAmountCoinValue = buy.amount
//           coin.buyAmountPriceValue = buy.cost
//           coin.buyAtValue = buy.price
//           console.log(buy)
//           console.log('\n\n************************************************')
//           console.log(`wallet: ${coin.myWallet} usd`)
//           console.log('---------------------------------')
//           console.log(
//             `buy coin ${coin.coinName}: ${coin.todayValue} usd, amount: ${coin.buyAmountCoinValue} coins, amount: ${coin.buyAmountPriceValue} usd, fee: ${getFee}`
//           )
//           updateProfileCoin(coin)
//         }
//       } catch (e) {
//         console.log(e)
//       }
//     }
//     // Check should sell ?
//     if (
//       coin.isBuy &&
//       coin.todayValue >= coin.minSellValue &&
//       coin.buyAmountCoinValue > 0
//     ) {
//       // Exchange order
//       try {
//         const sell = await setting.exchange.createOrder(
//           coin.symbolCode,
//           'market',
//           'sell',
//           coin.buyAmountCoinValue
//         )
//         if (sell) {
//           console.log(
//             'SELL',
//             coin.symbolCode,
//             coin.buyAmountCoinValue,
//             coin.minSellValue
//           )
//           createTransact(sell)
//           coin.isBuy = false
//           const getMoney = coin.todayValue * coin.buyAmountCoinValue
//           // Stack profit
//           const getProfit = Math.abs(coin.buyAmountPriceValue - getMoney)
//           const getFee = percentage(coin.feeValue, getProfit)
//           const getProfitWithFee = Math.abs(getProfit - getFee)
//           coin.profitStack += getProfitWithFee
//           coin.myWallet = Math.abs(coin.myWallet + getMoney - getFee)
//           console.log(sell)
//           console.log('---------------------------------')
//           console.log(
//             `sell at: ${coin.todayValue} usd, min sell amonth: ${coin.minSellValue} usd, coins: ${coin.buyAmountCoinValue}, receive: ${getMoney} usd`
//           )
//           console.log('---------------------------------')
//           console.log(
//             `available wallet: ${coin.myWallet}, profit stack: ${coin.profitStack}, fee: ${getFee}`
//           )
//           coin.buyAmountCoinValue = 0
//           coin.buyAmountPriceValue = 0
//           coin.buyAtValue = 0
//           coin.minSellValue = 0
//           updateProfileCoin(coin)
//         }
//       } catch (e) {
//         console.log(e)
//       }
//     }
//   }
// }

const botTradeV2 = async coin => {
  console.log(
    `Coin: ${coin.coinName}, price: ${coin.todayValue} usd, low: ${coin.todayLowValue} usd, hight: ${coin.todayHightValue} usd, sell at: ${coin.minSellValue} usd`
  )
  // return
  // Update price
  updateProfilePriceCoin(coin)
  // const d = new Date()
  // Mock trade today value
  // coin.todayValue = Math.floor(Math.random() * coin.todayHightValue)
  // coin.todayValue += 0.05
  // if (coin.todayValue >= coin.todayHightValue) return
  if (coin.todayValue <= 0) return
  // End mock value
  // Check should buy ?
  // And buy between low and height per day
  // And buy when rate match
  const getRateAmount = percentage(
    coin.buyRateSupplyPercentage,
    coin.todayHightValue
  )
  if (
    coin.myWallet > 0 &&
    coin.todayValue >= coin.todayLowValue &&
    coin.todayValue <= coin.todayHightValue &&
    !coin.isBuy
  ) {
    if (getRateAmount !== 0 && coin.todayValue >= getRateAmount) return
    // Get amount user buy
    const convertBuytoAmount = percentage(
      coin.buyAmountPercentage,
      coin.myWallet
    )
    const getFee = percentage(coin.feeValue, convertBuytoAmount)
    coin.myWallet = Math.abs(coin.myWallet - convertBuytoAmount)
    coin.buyAtValue = coin.todayValue
    coin.buyAmountPriceValue = Math.abs(convertBuytoAmount - getFee)
    coin.buyAmountCoinValue = Math.abs(
      (convertBuytoAmount - getFee) / coin.todayValue
    )
    coin.isBuy = true
    let minSellValue = percentage(coin.profitPercentage, coin.buyAtValue)
    minSellValue = Math.abs(minSellValue + coin.buyAtValue)
    coin.minSellValue = minSellValue
    // Exchange order
    try {
      const buy = await setting.exchange.createOrder(
        coin.symbolCode,
        'market',
        'buy',
        coin.buyAmountCoinValue
      )
      if (buy) {
        console.log(
          'BUY',
          coin.symbolCode,
          coin.buyAmountCoinValue,
          coin.todayValue
        )
        createTransact(buy)
        coin.buyAmountCoinValue = buy.amount
        coin.buyAmountPriceValue = buy.cost
        coin.buyAtValue = buy.price
        console.log(buy)
        console.log('\n\n************************************************')
        console.log(`wallet: ${coin.myWallet} usd`)
        console.log('---------------------------------')
        console.log(
          `buy coin ${coin.coinName}: ${coin.todayValue} usd, amount: ${coin.buyAmountCoinValue} coins, amount: ${coin.buyAmountPriceValue} usd, fee: ${getFee}`
        )
        updateProfileCoin(coin)
      }
    } catch (e) {
      console.log(e)
    }
  }
  // Check should sell ?
  if (
    coin.isBuy &&
    coin.todayValue >= coin.minSellValue &&
    coin.buyAmountCoinValue > 0
  ) {
    // Exchange order
    try {
      const sell = await setting.exchange.createOrder(
        coin.symbolCode,
        'market',
        'sell',
        coin.buyAmountCoinValue
      )
      if (sell) {
        console.log(
          'SELL',
          coin.symbolCode,
          coin.buyAmountCoinValue,
          coin.minSellValue
        )
        createTransact(sell)
        coin.isBuy = false
        const getMoney = coin.todayValue * coin.buyAmountCoinValue
        // Stack profit
        const getProfit = Math.abs(coin.buyAmountPriceValue - getMoney)
        const getFee = percentage(coin.feeValue, getProfit)
        const getProfitWithFee = Math.abs(getProfit - getFee)
        coin.profitStack += getProfitWithFee
        coin.myWallet = Math.abs(coin.myWallet + getMoney - getFee)
        console.log(sell)
        console.log('---------------------------------')
        console.log(
          `sell at: ${coin.todayValue} usd, min sell amonth: ${coin.minSellValue} usd, coins: ${coin.buyAmountCoinValue}, receive: ${getMoney} usd`
        )
        console.log('---------------------------------')
        console.log(
          `available wallet: ${coin.myWallet}, profit stack: ${coin.profitStack}, fee: ${getFee}`
        )
        coin.buyAmountCoinValue = 0
        coin.buyAmountPriceValue = 0
        coin.buyAtValue = 0
        coin.minSellValue = 0
        updateProfileCoin(coin)
      }
    } catch (e) {
      coin.minSellValue += 0.5
      updateProfileCoin(coin)
      console.log(e)
    }
  }
}

const percentage = (partialValue, totalValue) => {
  return (partialValue / 100) * totalValue
}

const updateProfileCoin = coin => {
  setting.dbDriver
    .collection('profile')
    .findOne({ _id: coin._id }, (err, result) => {
      if (result) {
        const newData = { ...coin, updatedAt: new Date() }
        delete newData.createdAt
        const saveData = { $set: newData }
        setting.dbDriver
          .collection('profile')
          .updateOne({ _id: coin._id }, saveData, { upsert: true })
      }
    })
}

const updateProfilePriceCoin = coin => {
  setting.dbDriver
    .collection('profile')
    .findOne({ _id: coin._id }, (err, result) => {
      if (result) {
        const newData = {
          todayHightValue: coin.todayHightValue,
          todayLowValue: coin.todayLowValue,
          todayValue: coin.todayValue,
          updatedAt: new Date()
        }
        delete newData.createdAt
        const saveData = { $set: newData }
        setting.dbDriver
          .collection('profile')
          .updateOne({ _id: coin._id }, saveData, { upsert: true })
      }
    })
}

const createTransact = transact => {
  setting.dbDriver.collection('transact').insertOne(transact, (err, result) => {
    if (err) throw err
    console.log('1 transact inserted.')
  })
}

// Bootstrap app
const Bootstrap = () => {
  setupDatabase()
  // setInterval(() => {
  //   botTrade()
  // }, setting.speed * 1000)
}

Bootstrap()
