module.exports = {
  apps: [
    {
      name: 'bot trade rebase',
      script: './rebase.js',
      env_production: {
        NODE_ENV: 'production',
      },
      env_development: {
        NODE_ENV: 'development',
      },
    },
  ],
};
