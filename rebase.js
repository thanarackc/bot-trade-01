const webSocket = require('ws');
const mongoClient = require('mongodb').MongoClient;
const dayjs = require('dayjs');
const cCxt = require('ccxt');

// App setting
let profile = {
  profileId: 'thanarak-busd',
  wallet: 0, // Total wallet amount. (BUSD preferred)
  spotWallet: 10, // Your wallet that use for buy each transaction. (Should more than 10)
  coin: '', // Hold a coin ie. doge, eth, btc.
  totalToken: 0, // Amount token you have.
  isBuy: false, // Flag status of buy.
  buyPrice: 0, // Price that bot buy.
  sellPrice: 0, // Price that bot should sell.
  profitPercentage: 1.25, // Profit percentage that want to take. 1.25 is normal rate.
  bestPricePercentage: 0, // Best when current price less than best percentage of all time height. 0 = buy any price. propose to defend against when price is low than ath
  timestamp: 0, // Date make order
};

// App config
const config = {
  binanceApi:
    'mjH4Sk0jNRx9erRz72MDD8DGyhPdKHkjjtrXxU5DzWFxF5cauYrqkgJs8PbFdTXb',
  binanceApiSecrete:
    '7I77YuadpAVFwHoMfc811SSH1Yleclxhyl6IRUzMfjfipTw8H42DyffHq9Pmzz0P',
  binanceSocketStreams:
    'wss://testnet.binance.vision/stream?streams=!ticker@arr',
  binanceUrlApi: 'https://testnet.binance.vision',
  userEmail: 'thanarackc@gmail.com',
  databaseUrl:
    'mongodb://root:rootpassword@206.189.38.110:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',
  databaseName: 'bot-trade-02',
  feeRate: 0.1, // Binance fee rate
};

var dbo;
/** @type {cCxt.binance} */
var exchange;
var coin; // Coin user buy data
var topCoin; // Top change coins

const setupDatabase = async () => {
  const url = config.databaseUrl;
  return new Promise((resolve) => {
    mongoClient.connect(
      url,
      { useUnifiedTopology: true },
      async function (err, db) {
        if (err) throw err;
        dbo = db.db(config.databaseName);
        console.log('Connected database.');
        resolve(true);
      }
    );
  });
};

const setupExchange = async () => {
  return new Promise((resolve) => {
    const exchangeId = 'binance';
    const exchangeClass = cCxt[exchangeId];
    const exchangeConnect = new exchangeClass({
      apiKey: config.binanceApi,
      secret: config.binanceApiSecrete,
      timeout: 30000,
      enableRateLimit: true,
    });
    exchange = exchangeConnect;
    exchange.setSandboxMode(true);
    console.log('Connected binance account.');
    resolve(true);
  });
};

const coinPriceUpdate = async () => {
  return new Promise((resolve) => {
    const ws = new webSocket(config.binanceSocketStreams);
    ws.on('open', function () {
      console.log('Connected binance socket.');
      resolve(true);
    });
    ws.on('message', function (data) {
      if (data) {
        // Start convert binary data to JSON
        const dataBuffer = Buffer.from(data, 'binary');
        const dataJson = JSON.parse(dataBuffer.toString());
        const dataList = dataJson.data;
        // Buy only market BUSD
        const filterBUSD = dataList.filter(
          (value) => value.s.substr(-4) === 'BUSD'
        );
        if (filterBUSD.length > 0) {
          // Sorting by height change and limit of 1 items
          const sortByHeightChange = filterBUSD.sort((a, b) => +b.P - +a.P);
          sortByHeightChange.length = Math.min(sortByHeightChange.length, 1);
          topCoin = sortByHeightChange[0];
          // Set coin data that user buy
          if (profile.isBuy) {
            const findCoin = filterBUSD.find(
              (value) => value.s === profile.coin
            );
            if (findCoin) {
              coin = findCoin;
            }
          }
        }
      }
    });
  });
};

const percentage = (partialValue, totalValue) => {
  return (100 * partialValue) / totalValue;
};

const minusPercent = (n, p) => {
  return n - n * (p / 100);
};

const plusPercent = (n, p) => {
  return n + n * (p / 100);
};

const onBuy = async () => {
  try {
    // Start buy top coin at current price
    const currentPrice = parseFloat(topCoin.c);
    let bestPrice = minusPercent(topCoin.h, profile.bestPricePercentage);
    if (profile.bestPricePercentage <= 0) bestPrice = currentPrice;
    // Buy when current price less than all time height 1%;
    if (currentPrice < bestPrice) return;
    // Check status buy should not buy
    if (
      !profile.isBuy &&
      profile.spotWallet > 0 &&
      profile.wallet >= profile.spotWallet &&
      profile.profitPercentage > 0 &&
      topCoin
    ) {
      coin = topCoin;
      // Wallet balance
      const spendBUSD = profile.spotWallet;
      // Buy in exchange
      const result = await exchange.createMarketOrder(coin.s, 'buy', 0, 0, {
        quoteOrderQty: spendBUSD,
      });
      if (result) {
        // Buy a coin with current price and setup sell price with profit
        profile.isBuy = true;
        profile.coin = coin.s;
        // Set price should be sell
        profile.sellPrice = plusPercent(result.price, profile.profitPercentage);
        profile.buyPrice = result.price;
        profile.timestamp = result.timestamp;
        profile.totalToken = result.amount;
        // Update information into database.
        await accountInformation();
        // Save transaction
        await saveTransaction();
        // Print log information
        console.log(
          '----------------\r\n Buy coin: %s, price: %s, sell at price: %s, spend: %s BUSD, total tokens: %s \r\n----------------',
          profile.coin,
          profile.buyPrice,
          profile.sellPrice,
          result.cost,
          profile.totalToken
        );
      }
    }
  } catch (e) {
    console.log(e);
    errorLog('error', e.message);
  }
};

const onSell = async () => {
  try {
    // Check status buy should buy
    if (profile.isBuy && coin && profile.totalToken) {
      // Get coin information that user buy
      const currentPrice = parseFloat(coin.c);
      // const diffDays = dayjs().diff(profile.timestamp, 'hours');
      // Check if sellPrice equal currentPrice then sell this order.
      // and check or when order holds more than 12 hours then should force sell.
      if (currentPrice >= profile.sellPrice) {
        // Wallet receive balance
        const result = await exchange.createMarketOrder(
          coin.s,
          'sell',
          profile.totalToken
        );
        if (result) {
          profile.timestamp = result.timestamp;
          // Update information into database.
          await accountInformation();
          // Save transaction
          await saveTransaction();
          // Reset wallet
          profile.coin = '';
          profile.isBuy = false;
          profile.buyPrice = 0;
          profile.sellPrice = 0;
          profile.totalToken = 0;
          // Reset current coin
          const lastActiveCoin = coin;
          coin = undefined;
          // Print log information
          console.log(
            '----------------\r\n Sell coin: %s, price: %s\r\n----------------',
            lastActiveCoin.s,
            currentPrice
          );
        }
      }
    }
  } catch (e) {
    console.log(e);
    errorLog('error', e.message);
  }
};

const onInformationWallet = async () => {
  if (!profile.isBuy) {
    console.log(
      '----------------\r\n Matching best price, top coin: %s, ath: %s, best price: %s, price: %s \r\n----------------',
      topCoin.s,
      topCoin.h,
      minusPercent(topCoin.h, profile.bestPricePercentage),
      topCoin.c
    );
  } else {
    console.log(
      'status: %s, wallet: %s, coin: %s, changes: %s, coin price: %s, buy price: %s, sell price: %s',
      profile.isBuy ? 'Wait sell' : 'Wait buy',
      Number(profile.wallet).toFixed(2) + ' BUSD',
      profile.coin || 'No',
      (coin?.P || 0) + '%',
      coin?.c || 0,
      profile.buyPrice || 0,
      profile.sellPrice || 0
    );
  }
};

const intervalCommands = () => {
  console.log('App started.');
  return setInterval(() => {
    // Start logic here.
    onBuy();
    onSell();
    onInformationWallet();
    // End logic
  }, 1500);
};

const accountInitial = async () => {
  try {
    const account = await dbo
      .collection('profile')
      .findOne({ profileId: profile.profileId });
    if (account) profile = { ...profile, ...account };
    console.log(
      '----------------\r\n Initial account data \r\n----------------'
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
};

const accountInformation = async () => {
  try {
    // Get wallet
    const wallet = await exchange.fetchBalance();
    profile.wallet = wallet.free.BUSD;
    console.log(
      '----------------\r\n Get account information \r\n----------------'
    );
    // Save data to mongodb
    await buyAndSellUpdateProfile();
  } catch (err) {
    console.log(err);
    throw err;
  }
};

const buyAndSellUpdateProfile = async () => {
  return await dbo.collection('profile').findOneAndUpdate(
    { profileId: profile.profileId },
    { $set: profile },
    {
      upsert: true,
    }
  );
};

const errorLog = async (typeError = 'error', error) => {
  return await dbo.collection('logs').insertOne({
    type: typeError,
    error,
    profile,
  });
};

const saveTransaction = async () => {
  return await dbo
    .collection('transaction')
    .insertOne({ ...profile, typeSide: profile.isBuy ? 'buy' : 'sell' });
};

const bootstrapApp = async () => {
  await setupDatabase();
  await setupExchange();
  await coinPriceUpdate();
  await accountInitial();
  await accountInformation();
  intervalCommands();
};

// App initial start
bootstrapApp();

/**
 * Step of this bot.
 * [1] Start get height of price.
 * [2] Buy a coin.
 *    [2.1] Execute when below -1% of current price of top coins in market.
 *    [2.2] isBuy should be false.
 *    [2.3] When current price less than 2% of all time height;
 * [3] Set sell price and stop buy order.
 * [4] Sell when current price reaches to (sell price) with take profit at 1.25%
 * [5] Back to step 1
 **/
